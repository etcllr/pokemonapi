# PokemonAPI

This is a simple API for managing Pokemon data. It's built with Node.js, Express, Sequelize, and SQLite.

## Usage
To install dependencies, use the following command : 
`npm install`.<br>
To start the server, use the following command:
`npm start`.<br><br>
The API has the following endpoints:

- `GET /pokemons`: Fetch all pokemons.
- `GET /pokemons/:id`: Fetch a specific pokemon by its ID.
- `POST /pokemons`: Create a new pokemon. The request body should include `name`, `type`, and `level`.
- `PUT /pokemons/:id`: Update a specific pokemon by its ID. The request body can include `name`, `type`, and `level`.
- `DELETE /pokemons/:id`: Delete a specific pokemon by its ID.

Don't forget to use the Bearer token : `OP58L89sClQom0AJPpbBJuQSX2bW23EX`

## Storage
Datas are stored in `storage/database.sqlite`.

Here the datas stocked in SQLite :

| ID | name       | type   | level |
|----|------------|--------|-------|
| 3  | Bulbasaur  | Grass  | 5     |
| 4  | Charmander | Fire   | 5     |
| 5  | Squirtle   | Water  | 5     |


## Contributing
This project is developed by **Nicolas Martinelli** (nicolas.martinelli@next-u.fr) and **Etienne Cellier** (etienne.cellier@next-u.fr).