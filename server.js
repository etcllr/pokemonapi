const { Sequelize, DataTypes } = require('sequelize');
const express = require('express');
const app = express();

require('dotenv').config();

const bearerTokenMiddleware = (req, res, next) => {
    const token = req.headers['authorization'] ? req.headers['authorization'].split(' ')[1] : null;

    if (token === process.env.ACCESS_TOKEN_SECRET) {
        next();
    } else {
        res.status(401).json({ message: 'Unauthorized: Invalid token' });
    }
};

app.use(bearerTokenMiddleware);

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "storage/database.sqlite",
});
const Pokemon = sequelize.define('Pokemon', {
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    level: DataTypes.INTEGER,
});

sequelize.sync()

app.use(express.json());

app.get('/pokemons', async (req, res) => {
    const pokemon = await Pokemon.findAll();
    return res.json(pokemon);
});

app.get('/pokemons/:id', async (req, res) => {
    const pokemon = await Pokemon.findByPk(req.params.id);
    if (!pokemon) {
        console.log(`Aucun Pokémon trouvé avec l'ID: ${req.params.id}`);
        return res.status(400).json({
            message: "This pokemon doesn't exist"
        });
    }
    return res.json(pokemon);
});

app.put('/pokemons/:id', async (req, res) => {
    const pokemon = await Pokemon.findByPk(req.params.id);
    if (!pokemon) {
        return res.status(400).json({
            message: "This pokemon doesn't exist"
        })
    }
    pokemon.name = req.body.name;
    pokemon.type = req.body.type;
    pokemon.level = req.body.level;
    await pokemon.save();
    return res.json(pokemon);
});

app.post('/pokemons', async (req, res) => {
    if (!req.body.name) {
        return res.status(400).json({
            message: "name parameters is missing"
        })
    }
    if (!req.body.type) {
        return res.status(400).json({
            message: "type parameters is missing"
        })
    }
    if (!req.body.level) {
        return res.status(400).json({
            message: "level parameters is missing"
        })
    }
    const pokemon = await Pokemon.create(req.body);
    return res.json(pokemon);
});

app.delete('/pokemons/:id', async (req, res) => {
    const pokemon = await Pokemon.findByPk(req.params.id);
    if (!pokemon) {
        return res.status(400).json({
            message: "This pokemon doesn't exist"
        })
    }
    await pokemon.destroy();
    return res.json(pokemon);
});

app.listen(3000);